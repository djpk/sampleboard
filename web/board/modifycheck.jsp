<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%--
  Created by IntelliJ IDEA.
  Date: 2017-07-11
  Time: 오전 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    if (request.getParameter("cat") == null || request.getParameter("page") == null || request.getParameter("no") == null) {
        out.println("<script type=\"text/javascript\">");
        out.println("alert('잘못된 요청입니다. 메인으로 이동합니다.');");
        out.println("</script>");
        response.sendRedirect("/");
    }

    String url = "/getAuth?do=modify&cat=" + request.getParameter("cat") + "&no=" + request.getParameter("no") + "&page=" + request.getParameter("page");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
  <%@include file="../commons/header.jsp"%>
  <title>Top navbar example for Bootstrap</title>
  <!-- Custom styles for this template -->
  <link href="resources/css/py_custom.css" rel="stylesheet">
</head>
<body>
<%@include file="../commons/navbar.jsp"%>

<div class="container">
    <csrf:form class="form-signin" method="post" action="<%=url%>">
        <h2 class="form-signin-heading">글 수정</h2>
        <% if (UserSession.getAttribute("userid") != null) { %>
        <p class="text-center"><%=UserSession.getAttribute("usernickname")%>으로 로그인되어 있습니다.</p>
        <input name="postPassword" type="hidden" value="trusted">
        <% } else { %>
        <label for="inputPassword" class="sr-only">비밀번호</label>
        <input name="postPassword" type="password" id="inputPassword" class="form-control" placeholder="게시글 암호를 입력하세요..." required>
        <% } %>
        <button class="btn btn-lg btn-primary btn-block" type="submit">수정하기</button>
</div>
</csrf:form>
</div>
<%@include file="../commons/bootstrap-js.jsp"%>
</body>
</html>
