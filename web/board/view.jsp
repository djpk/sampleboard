<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%@ page import="com.webboard.main.db.EnvHandler" %>
<%@ page import="com.webboard.main.db.dao.CommentDAO" %>
<%@ page import="com.sleepycat.persist.EntityCursor" %>
<%@ page import="com.webboard.main.db.entity.Comment" %>
<%@ page import="static com.sleepycat.je.CursorConfig.READ_UNCOMMITTED" %>
<%@ page import="java.util.HashMap" %>
<%--
  Created by IntelliJ IDEA.
  Date: 2017-07-11
  Time: 오후 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@include file="../commons/header.jsp"%>
    <title><%=request.getAttribute("subject")%></title>
    
    <!-- Custom styles for this template -->
    <link href="resources/css/py_custom.css" rel="stylesheet">
</head>

<body>
<%@include file="../commons/navbar.jsp"%>

<div class="container">


    <div class="row viewpost-header" style="margin-top: 20px;">
        <div class="col-12 viewpost-subject border border-left-0 border-right-0"><%=request.getAttribute("subject")%></div>
        <div class="w-100"></div>
        <div class="col-6 viewpost-info text-left">
            <% if (!request.getAttribute("trusted").equals(-1)) { %>
            <span class="loggined"><%=request.getAttribute("author")%></span>
            <% } else { %>
            <span><%=request.getAttribute("author")%></span>&nbsp;<span>(<%=request.getAttribute("ip")%>)</span>
            <% } %></div>
        <div class="col-6 viewpost-info text-right"><%=request.getAttribute("date")%></div>
    </div>

    <div class="viewpost-content no-gutters">
        <%=request.getAttribute("content")%>
    </div>

    <HR>

    <div id="commentList">
    </div>

    <csrf:form name="commentForm" id="commentForm" method="post" action="/board/comment_write" class="form-horizontal" role="form">
        <input type="hidden" name="postno" value="<%=request.getAttribute("postno")%>">
        <input type="hidden" name="page" value="<%=request.getAttribute("page")%>">
        <input type="hidden" name="category" value="<%=request.getAttribute("category")%>" />

        <div class="form-row">
            <div class="col">
                <textarea name="commentContent" class="viewpost-commentBox" id="commentContent" rows="5"></textarea>
            </div>
        </div>
        <div class="form-row" style="margin-top: 5px;">

            <% if (UserSession.getAttribute("userid") != null) { %>
            <input type="hidden" name="commentAuthor" id="commentAuthor" value="trusted" />
            <input type="hidden" name="commentPassword" id="commentPassword" value="trusted" />
            <div class="col-12 text-right">
                    <%--<button id="submitbtn" class="btn btn-success" onclick="this.disabled=true;onSubmit();" disabled>댓글작성</button>--%>
                <button type="button" id="submitbtn" class="btn btn-success" onclick="onSubmit();">작성하기</button>
            </div>
            <% } else { %>
            <div class="col-3">
                <input name="commentAuthor" id="commentAuthor" type="text" class="form-control" placeholder="이름">
            </div>
            <div class="col-3">
                <input name="commentPassword" id="commentPassword" type="password" class="form-control" placeholder="암호">
            </div>
            <div class="col-6 text-right">
                    <%--<button id="submitbtn" class="btn btn-success" onclick="this.disabled=true;onSubmit();" disabled>댓글작성</button>--%>
                <button type="button" id="submitbtn" class="btn btn-success" onclick="onSubmit();">작성하기</button>
            </div>
            <% } %>
        </div>
        <%--<div class="form-group">--%>
        <%--<div class="col-sm-1"></div>--%>
        <%--<div class="g-recaptcha col-sm-3" data-sitekey="6Ldv5ykUAAAAAPMF9pPl23mvjCoDEoXEIz21cH3E" data-callback="recaptcha"></div>--%>
        <%--</div>--%>
    </csrf:form>

    <div class="row viewpost-mani">
        <div class="col-6 text-left">
            <button type="button" class="btn btn-secondary" onclick="javascript:self.location='/board/modifycheck.jsp?cat=<%=request.getAttribute("category")%>&no=<%=request.getAttribute("postno")%>&page=<%=request.getAttribute("page")%>'">글 수정</button>
            <button style="margin-left: 10px;" type="button" class="btn btn-secondary" disabled>글 삭제</button>
        </div>
        <div class="col-6 text-right">
            <button type="button" class="btn btn-dark" onclick="javascript:self.location='/?cat=<%=request.getAttribute("category")%>&page=<%=request.getAttribute("page")%>'">목록보기</button>
        </div>
    </div>

    <jsp:include page="/list">
        <jsp:param name="cat" value="<%=request.getAttribute(\"category\")%>" />
        <jsp:param name="postno" value="<%=request.getAttribute(\"postno\")%>" />
        <jsp:param name="page" value="<%=request.getAttribute(\"page\")%>" />
    </jsp:include>

</div>
    <script language="javascript">
        var httpRequest;

        if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }

        // Mozilla, Opera, Webkit
        if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", function () {
                document.removeEventListener("DOMContentLoaded", arguments.callee, false);
                commentList(<%=request.getParameter("no")%>, 0);
            }, false);

        }
        // Internet Explorer
        else if (document.attachEvent) {
            document.attachEvent("onreadystatechange", function () {
                if (document.readyState === "complete") {
                    document.detachEvent("onreadystatechange", arguments.callee);
                    commentList(<%=request.getAttribute("no")%>, 0);
                }
            });
        }

        function onSubmit()
        {
            if (!document.getElementById('commentAuthor').value || document.getElementById('commentAuthor').value.length > 8)
            {
                alert('이름을 입력하지 않았거나, 9자 미만으로 입력해주세요.');
                return false;
            }
            if (!document.getElementById('commentPassword').value || document.getElementById('commentPassword').value.length > 16)
            {
                alert('암호를 입력하지 않았거나, 20자리 미만으로 입력해주세요.');
                return false;
            }
            if (!document.getElementById('commentContent').value || document.getElementById('commentContent').value.length > 300)
            {
                alert('댓글을 입력하지 않았거나, 300자 미만으로 입력해주세요.');
                return false;
            }
            document.getElementById('commentForm').submit();
        };
        function recaptcha() {
            document.getElementById('submitbtn').removeAttribute('disabled');
        };
        function commentList(postno, commentPage)
        {
            httpRequest.onreadystatechange = updateCommentList;
            httpRequest.open("POST", "/board/comment_list", true);
            httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
            httpRequest.send("postno=" + postno + "&commentPage=" + commentPage);
        }
        function updateCommentList() {
            if (httpRequest.readyState == 4 && httpRequest.status == 200) {
                document.getElementById("commentList").innerHTML = httpRequest.responseText;
            }
        }
    </script>
<%@include file="../commons/bootstrap-js.jsp"%>
</body>
</html>
