<%@ page import="java.util.HashMap" %>
<%@ page import="static com.webboard.main.web.InitialServlet.MAX_UPLOAD_GUEST" %>
<%@ page import="static com.webboard.main.web.InitialServlet.MAX_UPLOAD_MEMBER" %>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%--
  Created by IntelliJ IDEA.
  Date: 2017-07-11
  Time: 오전 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <%@include file="../commons/header.jsp"%>
    <title>Top navbar example for Bootstrap</title>
    <!-- Custom styles for this template -->
    <link href="resources/css/py_custom.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://cdn.ckeditor.com/4.7.2/standard-all/ckeditor.js"></script>
</head>
<body>
<% if (request.getAttribute("msg") != null) { %>
<div class="alert alert-danger txt_center" role="alert">
    <strong>오류!</strong> <%=request.getAttribute("msg")%>
</div> <% } %>
<%@include file="../commons/navbar.jsp"%>

<%
    if (request.getAttribute("postNo") == null) {
        out.println("<script type=\"text/javascript\">");
        out.println("alert('잘못된 요청입니다. 메인으로 이동합니다.');");
        out.println("</script>");
        response.sendRedirect("/");
    }

    int maxupload = MAX_UPLOAD_GUEST;

    if (UserSession.getAttribute("userid") != null) { //회원인 경우
        maxupload = MAX_UPLOAD_MEMBER;
    }
%>

<div class="container">

    <csrf:form name="postForm" id="postForm" method="post" action="/modify" class="form-horizontal" role="form">
        <input type="hidden" name="postNo" value="<%=request.getAttribute("postNo")%>" />
        <input type="hidden" name="postPage" value="<%=request.getParameter("page")%>" />
        <input type="hidden" name="postCategory" value="<%=request.getParameter("cat")%>" />
        <%
            if (UserSession.getAttribute("userid") != null) { %>
        <input type="hidden" name="postAuthor" id="postAuthor" value="trusted" />
        <input type="hidden" name="postOldPassword" id="postOldPassword" value="trusted" />
        <input type="hidden" id="postNewPassword" value="trusted" />
        <% } else { %>
        <div class="form-group row">
            <div class="col-md-4 col-12">
                <label for="postAuthor" class="col-form-label control-label">이름</label>
                <input name="postAuthor" id="postAuthor" type="text" class="form-control" placeholder="이름을 입력하세요..." value="<%=request.getAttribute("author")%>" required>
            </div>
            <div class="col-md-4 col-12">
                <label for="postOldPassword" class="col-form-label control-label">기존 암호</label>
                <input name="postOldPassword" id="postOldPassword" type="password" class="form-control" placeholder="암호를 입력하세요..." required>
            </div>
            <div class="col-md-4 col-12">
                <label for="postNewPassword" class="col-form-label control-label">새 암호</label>
                <input name="postNewPassword" id="postNewPassword" type="password" class="form-control" placeholder="기존 암호 유지시 빈칸">
            </div>
        </div>
        <% } %>
        <div class="form-group row">
            <div class="col-md-9 col-12">
                <label for="text" class="col-form-label control-label">제목</label>
                <input name="postSubject" id="postSubject" type="text" class="form-control form-control-sm" value="<%=request.getAttribute("subject")%>" placeholder="제목을 입력하세요..." required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <textarea name="editor" rows="50"><%=request.getAttribute("content")%></textarea>
                <script>
                    CKEDITOR.replace('editor', { customConfig: '/resources/ckeditor/config.js', filebrowserUploadUrl: '/upload', extraPlugins: 'image2,justify' });
                </script>
            </div>
        </div>
        <p>첨부파일 개수 : <span id="uploadcount">0</span> / <%=maxupload%>개</p>
        <ul id="attachment_list">
        </ul>
        <%--<div class="form-group">--%>
        <%--<div class="col-sm-1"></div>--%>
        <%--<div class="g-recaptcha col-sm-3" data-sitekey="6Ldv5ykUAAAAAPMF9pPl23mvjCoDEoXEIz21cH3E" data-callback="recaptcha"></div>--%>
        <%--</div>--%>
    </csrf:form>

    <form enctype="multipart/form-data" method="post" name="attachment">
        <input id="input_upload" type="file" name="upload" required />
        <button id="btn_upload" clas="btn btn-success">업로드</button>
        <span id="bar_upload" hidden><img src="/./resources/images/loader.gif"></span>
    </form>

    <div class="row viewpost-mani">
        <div class="col-6 text-left">
            <button type="button" class="btn btn-success" onclick="onSubmit_modify();">수정하기</button>
        </div>
        <div class="col-6 text-right">
            <button type="button" class="btn btn-dark" onclick="javascript:history.back(-1)">돌아가기</button>
        </div>
    </div>
    <%--<button id="submitbtn" class="btn btn-success" onclick="this.disabled=true;onSubmit();" disabled>작성하기</button>--%>
</div>
<%@include file="../commons/bootstrap-js.jsp"%>
<script src="/./resources/js/editor-form.js"></script>
<%
    String[] attachment_list = (String[]) request.getAttribute("postAttachments");
    if (attachment_list != null) {
        out.println("<script type=\"text/javascript\">");
        for (int i=0; i<attachment_list.length; i++) {
            out.println("addAttachment('" + attachment_list[i].substring(0, attachment_list[i].indexOf('#')) + "','" + attachment_list[i].substring(attachment_list[i].indexOf('#') + 1, attachment_list[i].indexOf('@')) + "','" + attachment_list[i].substring(attachment_list[i].indexOf('@') + 1, attachment_list[i].indexOf('!')) + "', 1);");
        }
        out.println("</script>");
    }
    String[] attachment_fucked_list = (String[]) request.getAttribute("postAttachments_fucked");
    if (attachment_fucked_list != null) {
        out.println("<script type=\"text/javascript\">");
        for (int i=0; i<attachment_fucked_list.length; i++) {
            out.println("addAttachment('" + attachment_fucked_list[i].substring(0, attachment_fucked_list[i].indexOf('#')) + "','" + attachment_fucked_list[i].substring(attachment_fucked_list[i].indexOf('#') + 1, attachment_fucked_list[i].indexOf('@')) + "','" + attachment_fucked_list[i].substring(attachment_fucked_list[i].indexOf('@') + 1, attachment_fucked_list[i].indexOf('!')) + "', 2);");
        }
        out.println("</script>");
    }
%>
</body>
</html>