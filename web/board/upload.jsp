<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%--
  Created by IntelliJ IDEA.
  Date: 2017-07-28
  Time: 오후 11:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title>File Upload</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<csrf:form method="POST" action="/upload" enctype="multipart/form-data" >
    <input type="hidden" name="CKEditorFuncNum" value="<%=request.getParameter("CKEditorFuncNum")%>" />
    File:
    <input type="file" name="file" id="file" /> <br/>
    </br>
    <input type="submit" value="Upload" name="upload" id="upload" />
</csrf:form>
</body>
</html>