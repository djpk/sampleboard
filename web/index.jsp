<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%--
  Created by IntelliJ IDEA.
  Date: 2017-07-11
  Time: 오전 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String category = "all", pageNum = "1";
    if (request.getParameter("cat") != null)
        category = request.getParameter("cat");

    if (request.getParameter("page") != null)
        pageNum = request.getParameter("page");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
  <%@include file="commons/header.jsp"%>
  <title>Top navbar example for Bootstrap</title>
  <!-- Custom styles for this template -->
  <link href="resources/css/py_custom.css" rel="stylesheet">
</head>
<body>
<%@include file="commons/navbar.jsp"%>

<div class="container">
  <jsp:include page="/list" >
    <jsp:param name="cat" value="<%=category%>" />
    <jsp:param name="page" value="<%=pageNum%>" />
  </jsp:include>

    <div class="col-12 text-right">
        <button type="button" class="btn btn-success" onclick="javascript:self.location='/board/write.jsp?cat=<%=category%>&page=<%=pageNum%>'">글쓰기</button>
    </div>
</div>
<%@include file="commons/bootstrap-js.jsp"%>
</body>
</html>
