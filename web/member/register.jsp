<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%--
  Created by IntelliJ IDEA.
  Date: 2017-08-08
  Time: 오후 5:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../commons/header.jsp"%>

    <title>회원 가입</title>
    <link href="../resources/css/py_custom.css" rel="stylesheet">
</head>
<% if (request.getAttribute("msg") != null) { %>
<div class="alert alert-danger txt_center" role="alert">
    <strong>오류!</strong> <%=request.getAttribute("msg")%>
</div> <% } %>
<body>
<%@include file="../commons/navbar.jsp"%>
<div class="container">
    <% if (UserSession.getAttribute("userid") != null) { %>
    <p class="text-center">이미 <%=UserSession.getAttribute("usernickname")%>으로 로그인되어 있습니다.</p>
    <% } else { %>
    <csrf:form class="form-register" method="post" action="/member/register">
        <h2 class="form-register-heading">회원가입</h2>

        <div class="form-group">
            <label class="form-control-label" for="registerId">로그인 ID</label>
            <input name="memberUsername" type="text" class="form-control" id="registerId" aria-describedby="registerIdhelp" value="<%=request.getAttribute("username") != null ? request.getAttribute("username") : ""%>" required>
            <small id="registerIdhelp" class="form-text text-muted">영문과 숫자만 가능합니다.<br>최소 4자 ~ 최대 20자, <b>중복 불가</b></small>
        </div>

        <div class="form-group">
            <label class="form-control-label" for="registerPassword">비밀번호</label>
            <input name="memberPassword" type="password" class="form-control" id="registerPassword" aria-describedby="registerPasswordhelp" required>
            <small id="registerPasswordhelp" class="form-text text-muted">최소 4자 ~ 최대 20자</small>
        </div>

        <div class="form-group">
            <label class="form-control-label" for="registerNickname">닉네임</label>
            <input name="memberNickname" type="text" class="form-control" id="registerNickname" aria-describedby="registerNicknamehelp" value="<%=request.getAttribute("nickname") != null ? request.getAttribute("nickname") : ""%>" required>
            <small id="registerNicknamehelp" class="form-text text-muted">최소 2자 ~ 최대 8자, 공백 불가, <b>중복 불가</b></small>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" value="registerToSAgreement"> 이용 약관에 동의합니다.
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">제출</button>

    </csrf:form>
    <% } %>
</div>
<%@include file="../commons/bootstrap-js.jsp"%>
</body>
</html>