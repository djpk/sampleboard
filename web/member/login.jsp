<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%--
  Created by IntelliJ IDEA.
  Date: 2017-08-08
  Time: 오후 5:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../commons/header.jsp"%>

    <title>로그인</title>
    <link href="../resources/css/py_custom.css" rel="stylesheet">
</head>
<body>
<% if (request.getAttribute("msg") != null) { %>
<div class="alert alert-danger txt_center" role="alert">
    <strong>오류!</strong> <%=request.getAttribute("msg")%>
</div> <% } %>
<%@include file="../commons/navbar.jsp"%>
<div class="container">
    <csrf:form class="form-signin" method="post" action="/member/login">
        <% if (UserSession.getAttribute("userid") != null) { %>
        <p class="text-center">이미 <%=UserSession.getAttribute("usernickname")%>으로 로그인되어 있습니다.</p>
        <% } else { %>
        <form class="form-signin">
            <h2 class="form-signin-heading">로그인</h2>
            <label for="inputUsername" class="sr-only">사용자 ID</label>
            <input name="loginUsername" type="text" id="inputUsername" class="form-control" required autofocus>
            <label for="inputPassword" class="sr-only">비밀번호</label>
            <input name="loginPassword" type="password" id="inputPassword" class="form-control" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">제출</button>
        </form>
        <% } %>
        </div>
    </csrf:form>
<%@include file="../commons/bootstrap-js.jsp"%>
</body>
</html>
