<%@ page import="com.webboard.main.db.EnvHandler" %>
<%@ page import="com.webboard.main.db.dao.AttachmentDAO" %>
<%@ page import="com.webboard.main.db.entity.Attachment" %>
<%@ page import="com.sleepycat.persist.EntityCursor" %><%--
  Created by IntelliJ IDEA.
  Date: 2017-07-11
  Time: 오후 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>
<%
    HttpSession session1 = request.getSession();
%>
<p><%=session1.getAttribute("username")%></p>
<p><%=session1.getAttribute("mailaddress")%></p>
<p><%=session1.getAttribute("nickname")%></p>
<p><%=session1.getAttribute("sessionuserid")%></p>
<p><%=session1.getAttribute("userauth")%></p>
<p><%=session1.getAttribute("nickname")%>님, 로그인 환영합니다.</p>

<p><%=session1.getAttribute("userid")%>님, 로그인 환영합니다.</p>
<p><%=session1.getId()%>님, 로그인 환영합니다.</p>

<%
    EnvHandler DbEnvHandler = new EnvHandler();
    DbEnvHandler.dbinit();

    DbEnvHandler.makeES(3);
    AttachmentDAO adao = new AttachmentDAO(DbEnvHandler.getES_attachments());

    EntityCursor<Attachment> attaches = adao.pidx.entities();

    try {
        for (Attachment Attach : attaches) {
            out.println(Attach.getAttach_id() + " || " + Attach.getAttach_fname() + " || " + Attach.getAttach_status() + "<br />");
        }
    } finally {
        attaches.close();
        DbEnvHandler.dbclose();
    }

%>

</body>
</html>