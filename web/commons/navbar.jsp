<%--
  Created by IntelliJ IDEA.
  Date: 2017-08-12
  Time: 오후 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% HttpSession UserSession = request.getSession(); %>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <a class="navbar-brand navbar-cs-sitename" href="/">Top navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/?cat=all&page=1">전체보기 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/?cat=apple&page=1">분류 A</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/?cat=banana&page=1">분류 B</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/?cat=cherry&page=1">분류 C</a>
            </li>
        </ul>
        <form class="form-inline mt-2 mt-md-0">
            <% if (UserSession.getAttribute("userid") != null) { %>
            <label style="color: white; margin-right: 10px;"><%=UserSession.getAttribute("usernickname")%>님 환영합니다.</label>
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="javascript:self.location='/member/logout'">로그아웃</button>
            <% } else { %>
            <button class="btn btn-outline-success my-2 my-sm-0" type="button" onclick="javascript:self.location='/member/login'">로그인</button>
            <button class="btn btn-outline-primary my-2 my-sm-0" type="button" style="margin-left: 10px;" onclick="javascript:self.location='/member/register'">회원가입</button>
            <% } %>
        </form>
    </div>
</nav>