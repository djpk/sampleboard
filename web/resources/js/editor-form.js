var form = document.forms.namedItem("attachment");
var progress_bar = document.getElementById('progress_bar');

form.addEventListener('submit', function(ev) {

    document.getElementById('btn_upload').setAttribute('disabled', '');
    document.getElementById('bar_upload').removeAttribute('hidden');
    document.getElementById('btn_upload').innerHTML = "업로드 중...";

    var attachdata = new FormData(form);
    var counts = $('#attachment_list li:visible').length;
    attachdata.append("currentupload", counts);
    var httpRequest;

    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpRequest.open("POST", "/upload", true);
    httpRequest.onload = function() {
        if (httpRequest.status == 200) {
            var originresponse = httpRequest.responseText;
            var fileid = originresponse.substring(0, originresponse.indexOf("#"));
            var fname = originresponse.substring(originresponse.indexOf("#") + 1, originresponse.indexOf("@"));
            var furl = originresponse.substring(originresponse.indexOf("@") + 1, originresponse.indexOf("!"));
            addAttachment(fileid, fname, furl, 0);
            ckeditorImageInsert(furl);
        } else {
            alert(httpRequest.responseText);
        }
        $("#input_upload").replaceWith($("#input_upload").val('').clone(true));
        document.getElementById('btn_upload').removeAttribute('disabled');
        document.getElementById('bar_upload').setAttribute('hidden', '');
        document.getElementById('btn_upload').innerHTML = "업로드";
    };

    httpRequest.send(attachdata);
    ev.preventDefault();
}, false);

function onSubmit()
{
    if (!document.getElementById('postAuthor').value || document.getElementById('postAuthor').value.length > 8)
    {
        alert('이름을 입력하지 않았거나, 9자 미만으로 입력해주세요.');
        return false;
    }
    if (!document.getElementById('postPassword').value || document.getElementById('postPassword').value.length > 16)
    {
        alert('암호를 입력하지 않았거나, 20자리 미만으로 입력해주세요.');
        return false;
    }
    if (!document.getElementById('postSubject').value || document.getElementById('postSubject').value.length > 32)
    {
        alert('제목을 입력하지 않았거나, 30자 미만으로 입력해주세요.');
        return false;
    }
    if (!CKEDITOR.instances.editor.getData())
    {
        alert('본문을 입력하세요.');
        return false;
    }
    document.getElementById('postForm').submit();
};
function onSubmit_modify()
{
    if (!document.getElementById('postAuthor').value || document.getElementById('postAuthor').value.length > 8)
    {
        alert('이름을 입력하지 않았거나, 9자 미만으로 입력해주세요.');
        return false;
    }
    if (!document.getElementById('postOldPassword').value || document.getElementById('postOldPassword').value.length > 16)
    {
        alert('이전 암호를 입력하지 않았거나, 20자리 미만으로 입력해주세요.');
        return false;
    }
    if (document.getElementById('postNewPassword').value.length > 16)
    {
        alert('새 암호는 20자리 미만으로 입력해주세요.');
        return false;
    }
    if (!document.getElementById('postSubject').value || document.getElementById('postSubject').value.length > 32)
    {
        alert('제목을 입력하지 않았거나, 30자 미만으로 입력해주세요.');
        return false;
    }
    if (!CKEDITOR.instances.editor.getData())
    {
        alert('본문을 입력하세요.');
        return false;
    }
    document.getElementById('postForm').submit();
};
function recaptcha() {
    document.getElementById('submitbtn').removeAttribute('disabled');
};
function addAttachment(fileid, fname, furl, isModify){
    var attachlist = document.getElementById("attachment_list");
    var attachelement = document.createElement("li");
    attachelement.setAttribute("id", "li_" + fileid);
    attachelement.setAttribute("class", "attachment_element");
    if (isModify == 2) {
        attachelement.setAttribute("hidden", "");
    }
    attachlist.appendChild(attachelement);
    var attachlabel = document.createElement("label");
    attachlabel.innerHTML = fname;
    attachelement.appendChild(attachlabel);
    var attachinput = document.createElement("input");
    attachinput.setAttribute("type", "hidden");
    if (isModify != 2) {
        attachinput.setAttribute("name", "saved");
    } else {
        attachinput.setAttribute("name", "fucked");
    }
    attachinput.setAttribute("value", fileid + "#" + fname + "@" + furl + "!");
    attachinput.setAttribute("id", fileid);
    attachelement.appendChild(attachinput);
    if (isModify != 2) {
        var insertbutton = document.createElement("button");
        insertbutton.setAttribute("type", "button");
        insertbutton.setAttribute("onclick", "ckeditorImageInsert('"+furl+"')");
        insertbutton.innerHTML = "삽입";
        attachelement.appendChild(insertbutton);
        var deletebutton = document.createElement("button");
        deletebutton.setAttribute("type", "button");
        deletebutton.innerHTML = "삭제";

        if (isModify == 0) {
            deletebutton.setAttribute("onclick", "$('#li_" + fileid + "').remove(); document.getElementById('uploadcount').innerHTML = $('#attachment_list li:visible').length");
        } else if (isModify == 1) {
            deletebutton.setAttribute("onclick", "document.getElementById('" + fileid + "').setAttribute('name', 'fucked'); document.getElementById('li_" + fileid + "').setAttribute('hidden',''); document.getElementById('uploadcount').innerHTML = $('#attachment_list li:visible').length");
        }
        attachelement.appendChild(deletebutton);
    }
    document.getElementById('uploadcount').innerHTML = $('#attachment_list li:visible').length;
};
function ckeditorImageInsert(furl) {
    var element = new CKEDITOR.dom.element('img');
    element.setAttribute('src', furl);
    CKEDITOR.instances.editor.insertElement(element);
};