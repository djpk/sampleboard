package com.webboard.main.exceptions;

/**
 * Created by dj on 2017-08-09.
 */
public class MsgCall extends Exception {
    public String msg = "";
    public int param = -1;
    public MsgCall(String errormsg, int errparam) { this.msg = errormsg; this.param = errparam; }
    public MsgCall(String errormsg) { this.msg = errormsg; }
}
