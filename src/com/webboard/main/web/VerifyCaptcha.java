package com.webboard.main.web;

import com.google.gson.stream.JsonReader;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;


public class VerifyCaptcha {

    private final String reCaptchaURL = "0000";
    private final String secretkey = "0000";
    private boolean isVerified;

    public boolean verifycode(String gRcaptcharesponse) throws IOException {
        if (gRcaptcharesponse == "" || gRcaptcharesponse == null) {
            return false;
        }
        try {
            URL googleUrl = new URL(reCaptchaURL);
            HttpsURLConnection googleConnection = (HttpsURLConnection) googleUrl.openConnection();
            googleConnection.setRequestMethod("POST");
            String params = "secret=" + secretkey + "&response=" + gRcaptcharesponse;

            googleConnection.setDoOutput(true);
            DataOutputStream sendstreams = new DataOutputStream(googleConnection.getOutputStream());
            sendstreams.writeBytes(params);
            sendstreams.flush();
            sendstreams.close();

            //JSON Parsing
            JsonReader reader = new JsonReader(new InputStreamReader(googleConnection.getInputStream(), "UTF-8"));

            reader.beginObject();
            reader.hasNext();
            String googleverified = reader.nextName();
            if (googleverified.equals("success")) {
                isVerified = reader.nextBoolean();
            }
            while (reader.hasNext()) { reader.skipValue(); }
            reader.endObject();
            reader.close();
            return isVerified;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
