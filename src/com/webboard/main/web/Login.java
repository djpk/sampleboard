package com.webboard.main.web;

import com.nhncorp.lucy.security.xss.XssPreventer;
import com.webboard.main.db.EnvHandler;
import com.webboard.main.db.entity.Member;
import com.webboard.main.db.dao.MemberDAO;
import com.webboard.main.exceptions.MsgCall;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.sleepycat.je.LockMode.READ_UNCOMMITTED;


@WebServlet(name = "Login", urlPatterns = "/member/login")
public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EnvHandler DbEnvHandler = new EnvHandler();
        try {
            String username = request.getParameter("loginUsername");
            String password = request.getParameter("loginPassword");

            username = XssPreventer.escape(username);

            DbEnvHandler.dbinit();
            DbEnvHandler.makeES(0);
            MemberDAO dao = new MemberDAO(DbEnvHandler.getES_members());
            Member theMember = dao.sidx_username.get(null, username, READ_UNCOMMITTED);

            if (theMember == null)
                throw new MsgCall("아이디 혹은 비밀번호가 틀립니다.");
            else if (theMember.checkMember_password(password)) {
                //로그인 성공!
                HttpSession session = request.getSession();
                session.setAttribute("userid", theMember.getMember_id());
                session.setAttribute("usernickname", theMember.getMember_nickname());
                session.setAttribute("userauth", theMember.getMember_authtoken());
                response.sendRedirect("/");
            } else //비밀번호 틀림!
                throw new MsgCall("아이디 혹은 비밀번호가 틀립니다.");
        } catch (MsgCall dpe) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/member/login.jsp");
            request.setAttribute("msg", dpe.msg);
            dispatcher.forward(request, response);
        } finally {
                DbEnvHandler.dbclose();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }
}
