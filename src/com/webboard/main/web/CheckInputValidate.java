package com.webboard.main.web;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

public class CheckInputValidate {
    /**
     * 글쓰기 내용 적합성 체크
     * - 범 례 -  (String).length는 영숫자 기준으로, getByte.length는 한글 n자*3byte 기준으로.
     * 필명(a:author)     - 한글 8자, 영숫자 14자 -- 14chars / 24bytes
     * 비번(p:password)   - 한글20자, 영숫자 20자 -- 20chars / 60bytes
     * 제목(s:subject)    - 한글30자, 영숫자 60자 -- 60chars / 90bytes
     * 내용(c:contents)   - 한글100K자           -- 300,000Bytes, 300KB
     */
    public static boolean checkWriteFormCheck(String a, String p, String s, String c, String g) throws UnsupportedEncodingException {

        if (InitialServlet.getDoDbWrite() != 1) {
            return false;
        }
        if (a.isEmpty() || p.isEmpty() || s.isEmpty() || c.isEmpty()) {
            return false;
        }
        if (a.length() > 14 || p.length() > 20 || s.length() > 60) {
            return false;
        }
        try { //UTF-8 한글:3BYTE 영문 1BYTE 8 24자
            if (a.getBytes("utf-8").length > 24) {// 3BYTE X 한글 8자
                return false;
            }
            else if (p.getBytes("utf-8").length > 60) {// AUTH CODE 36자리 * 2Byte
                return false;
            }
            else if (s.getBytes("utf-8").length > 90) {// 3BYTE X 한글 30자
                return false;
            }
            else if (c.getBytes("utf-8").length > 300000) {// 3BYTE X 한글 100,000자
                return false;
            }
            else {
                //VerifyCaptcha captcha = new VerifyCaptcha();
                //return captcha.verifycode(g);
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * 회원가입 적합성 체크
     * - 범 례 -  (String).length는 영숫자 기준으로, getByte.length는 한글 n자*3byte 기준으로.
     * 로긴(u:username)   - 영숫자only 20자      -- 20chars / 40bytes
     * 필명(n:nickname)   - 한글 8자, 영숫자 14자 -- 14chars / 24bytes
     * 메일(e:email)      - 영숫자only 40자      -- 40chars / 80bytes
     * 비번(p:password)   - 한글 20자, 영숫자20자 -- 20chars / 60bytes
     */
    public static boolean checkRegisterFormCheck(String u, String n, String p, String g) throws UnsupportedEncodingException {

        if (InitialServlet.getDoDbWrite() != 1)
            return false;

        if (!checkNumAlphaOnly(u, false))
            return false;

        if (u.isEmpty() || n.isEmpty() || p.isEmpty())
            return false;

        if ( u.length() > 20 || n.length() > 14 || p.length() > 20 )
            return false;

        if ( u.length() < 4 || n.length() < 2 || p.length() < 4 )
            return false;

        try { //UTF-8 한글:3BYTE 영문 1BYTE 8 24자
            if (u.getBytes("utf-8").length > 40) // 2BYTE X 영숫 only 40자
                return false;
            else if (n.getBytes("utf-8").length > 24) // 3BYTE X 한글 8자
                return false;
            else if (p.getBytes("utf-8").length > 60) // 3BYTE X 한글 20자
                return false;
            else {
                //VerifyCaptcha captcha = new VerifyCaptcha();
                //return captcha.verifycode(g);
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
    }


    /**
     * 댓글 내용 적합성 체크
     * - 범 례 -  (String).length는 영숫자 기준으로, getByte.length는 한글 n자*3byte 기준으로.
     * 필명(a:author)     - 한글 8자, 영숫자 14자 -- 14chars / 24bytes
     * 비번(p:password)   - 한글20자, 영숫자 20자 -- 20chars / 60bytes
     * 내용(c:contents)   - 한글300자           -- 300chars / 900bytes
     */
    public static boolean checkCommentFormCheck(String a, String p, String c, String g, int d) throws UnsupportedEncodingException {
        if (InitialServlet.getDoDbWrite() != 1 || d == 0)
            return false;
        if (a.isEmpty() || p.isEmpty() || c.isEmpty())
            return false;
        if (a.length() > 14 || p.length() > 20 || c.length() > 300)
            return false;
        try { //UTF-8 한글:3BYTE 영문 1BYTE 8 24자
            if (a.getBytes("utf-8").length > 24) // 3BYTE X 한글 8자
                return false;
            else if (p.getBytes("utf-8").length > 60)
                return false;
            else if (c.getBytes("utf-8").length > 900) // 3BYTE X 한글 300자
                return false;
            else {
                //VerifyCaptcha captcha = new VerifyCaptcha();
                //return captcha.verifycode(g);
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean checkNumAlphaOnly(String text, boolean isEmail) {
        char chrInput;
        for (int i = 0; i < text.length(); i++) {
            chrInput = text.charAt(i); // 입력받은 텍스트에서 문자 하나하나 가져와서 체크
            if (chrInput >= 0x61 && chrInput <= 0x7A) {
                // 영문(소문자) OK!
            }
            else if (chrInput >=0x41 && chrInput <= 0x5A) {
                // 영문(대문자) OK!
            }
            else if (chrInput >= 0x30 && chrInput <= 0x39) {
                // 숫자 OK!
            }
            else if (isEmail && (chrInput == 0x2E || chrInput == 0x40)) {
                // '.' 하고 '@'만 가능
            }
            else {
                return false;   // 영문자도 아니고 숫자도 아님!
            }
        }
        return true;
    }

    public static boolean checkViewGETdata(HttpServletRequest request) {
        return false;
    }

    public static boolean checkListGETdata(HttpServletRequest request) {
        return false;
    }
}
