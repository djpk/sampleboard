package com.webboard.main.web.backblazeb2;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.TimerTask;

import static com.webboard.main.web.InitialServlet.setBackblazeB2Apiurl;
import static com.webboard.main.web.InitialServlet.setBackblazeB2Authcode;
import static com.webboard.main.web.InitialServlet.setBackblazeB2Downloadurl;

/**
 * Created by dj on 2017-08-25.
 */
public class RenewB2Auth extends TimerTask {
    @Override
    public void run() {
        String accountID = "f92297b77d28";
        String applicationKey = "00115fd39d8a0064de874c840230bdcabad7c01575";
        String headerEncoded = "Basic " + Base64.getEncoder().encodeToString((accountID + ":" + applicationKey).getBytes());

        HttpURLConnection connection = null;
        try {
            URL url = new URL("https://api.backblazeb2.com/b2api/v1/b2_authorize_account");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", headerEncoded);

            //JSON Parsing
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(new InputStreamReader(connection.getInputStream(), "UTF-8"));

            setBackblazeB2Apiurl(element.getAsJsonObject().get("apiUrl").getAsString());
            setBackblazeB2Authcode(element.getAsJsonObject().get("authorizationToken").getAsString());
            setBackblazeB2Downloadurl(element.getAsJsonObject().get("downloadUrl").getAsString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
    }
}
