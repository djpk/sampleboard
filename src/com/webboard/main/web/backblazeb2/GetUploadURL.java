package com.webboard.main.web.backblazeb2;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.webboard.main.web.InitialServlet;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


public class GetUploadURL {
    private String bucketID = "cfd942925977bb37670d0218";
    private HttpURLConnection connection = null;
    private String postParams = "{\"bucketId\":\"" + bucketID + "\"}";
    byte postData[] = postParams.getBytes(StandardCharsets.UTF_8);


    public String[] getUrlandToken() throws IOException {
        String[] UrlandToken = new String[2];

        try {
            URL url = new URL(InitialServlet.getBackblazeB2Apiurl() + "/b2api/v1/b2_get_upload_url");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", InitialServlet.getBackblazeB2Authcode());
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", Integer.toString(postData.length));
            connection.setDoOutput(true);
            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.write(postData);

            //JSON Parsing
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(new InputStreamReader(connection.getInputStream(), "UTF-8"));

            UrlandToken[0] = element.getAsJsonObject().get("uploadUrl").getAsString();
            UrlandToken[1] = element.getAsJsonObject().get("authorizationToken").getAsString();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
            return UrlandToken;
        }
    }
}