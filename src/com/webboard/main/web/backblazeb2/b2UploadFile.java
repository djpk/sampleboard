package com.webboard.main.web.backblazeb2;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.webboard.main.db.EnvHandler;
import com.webboard.main.db.dao.AttachmentDAO;
import com.webboard.main.db.entity.Attachment;
import com.webboard.main.exceptions.MsgCall;
import com.webboard.main.web.InitialServlet;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;

import java.nio.file.Paths;


@WebServlet(name = "b2UploadFile", urlPatterns = "/upload")
@MultipartConfig
public class b2UploadFile extends HttpServlet {
    private final int MAX_FILE_SIZE = 1024 * 1024 * 2; // 2MB

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        final Part filePart = request.getPart("upload");
        InputStream filecontent = null;
        HttpSession UserSession = request.getSession();

        int currentupload = Integer.parseInt(request.getParameter("currentupload"));

        try {

            if (UserSession.getAttribute("userid") != null && currentupload >= InitialServlet.MAX_UPLOAD_MEMBER)
                throw new MsgCall("첨부파일은 최대 "+ InitialServlet.MAX_UPLOAD_MEMBER +"개까지 업로드 가능합니다.");
            else if (currentupload >= InitialServlet.MAX_UPLOAD_GUEST)
                throw new MsgCall("첨부파일은 최대 "+ InitialServlet.MAX_UPLOAD_GUEST +"개까지 업로드 가능합니다.");

            switch (filePart.getContentType()) {
                case "image/jpeg":
                    break;
                case "image/gif":
                    break;
                case "image/png":
                    break;
                case "image/bmp":
                    break;
                case "video/webm":
                    break;
                case "image/webp":
                    break;
                case "image/svg+xml":
                    break;
                default:
                    throw new MsgCall("이미지 파일만 업로드 가능 합니다.");
            }

            if (filePart.getSize() > MAX_FILE_SIZE)
                throw new MsgCall("첨부파일 용량이 너무 큽니다. 단일 파일당 2MB를 넘지않아야 합니다.");

            GetUploadURL getB2url = new GetUploadURL();
            String[] UrlAndToken = getB2url.getUrlandToken().clone();
            filecontent = filePart.getInputStream();

            HttpPost postconn = new HttpPost(UrlAndToken[0]);

            String filesha1 = sha1Hex(filecontent);

            String fname_origin = filePart.getHeader("Content-Disposition").replaceFirst("(?i)^.*filename=\"([^\"]+)\".*$", "$1");
            fname_origin = Paths.get(fname_origin).getFileName().toString();

            String fname_opt = filesha1 + "." + fname_origin.substring(fname_origin.lastIndexOf(".") + 1);

            postconn.addHeader("Authorization", UrlAndToken[1]);
            postconn.addHeader("X-Bz-File-Name", fname_opt);
            postconn.addHeader("X-Bz-Content-Sha1", filesha1);
            postconn.addHeader("Content-Type", filePart.getContentType());

            BasicHttpEntity fileEntity = new BasicHttpEntity();
            fileEntity.setContent(filePart.getInputStream());
            fileEntity.setContentLength(filePart.getSize());
            postconn.setEntity(fileEntity);

            CloseableHttpResponse httpresponse = httpclient.execute(postconn);

            EnvHandler DbEnvHandler = new EnvHandler();
            DbEnvHandler.dbinit();

            try {
                //JSON Parsing
                JsonParser parser = new JsonParser();
                JsonElement element = parser.parse(new InputStreamReader(httpresponse.getEntity().getContent(), "UTF-8"));
                String fileId = element.getAsJsonObject().get("fileId").getAsString();
                String fileurl = InitialServlet.getBackblazeB2Downloadurl().substring(0,13) + "backblazeb2.com/file/uploadimgs/" + fname_opt;

                DbEnvHandler.makeES(3);
                AttachmentDAO dao = new AttachmentDAO(DbEnvHandler.getES_attachments());
                Attachment theAttach = new Attachment();

                theAttach.setAttach_id(element.getAsJsonObject().get("fileId").getAsString());
                theAttach.setAttach_fname(fname_opt);
                theAttach.setAttach_status(0);

                dao.pidx.put(theAttach);

                PrintWriter out = response.getWriter();
                out.println(fileId + "#" + fname_origin + "@" + fileurl + "!");
            } finally {
                httpresponse.close();
                DbEnvHandler.dbclose();
            }

        } catch (MsgCall mcall) {
            response.setStatus(400);
            PrintWriter out = response.getWriter();
            out.println(mcall.msg);
        } catch (Exception e) {
            response.setStatus(500);
            e.printStackTrace();
            PrintWriter out = response.getWriter();
            out.println("업로드를 할 수 없습니다. 나중에 다시 시도해주세요.");
        } finally {
            if (filecontent != null)
                filecontent.close();

            httpclient.close();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
