package com.webboard.main.web;

import com.webboard.main.web.backblazeb2.RenewB2Auth;
import com.sleepycat.je.Durability;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.SequenceConfig;
import com.sleepycat.persist.StoreConfig;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;


@WebServlet(name = "InitialServlet")
public class InitialServlet extends HttpServlet {
    private static int doDbWrite = 1;

    public static int MAX_UPLOAD_GUEST = 5;
    public static int MAX_UPLOAD_MEMBER = 20;

    private static String BACKBLAZE_B2_AUTHCODE = "";
    private static String BACKBLAZE_B2_APIURL = "";
    private static String BACKBLAZE_B2_DOWNLOADURL = "";
    public static String PATH_TO_DB;
    public static EnvironmentConfig envConfig = null;
    public static StoreConfig ES_config = null;
    public static Durability duraConfig = null;
    public static SequenceConfig SeqConfig = null;

    private static Timer B2timer;
    private static TimerTask B2Task;

    @Override
    public void init() throws ServletException {

        PATH_TO_DB = getServletContext().getRealPath("/") + "database";

        File file = new File(PATH_TO_DB);
        if (!file.exists()) {
            file.mkdirs();
        }

        envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        envConfig.setCachePercent(50);

        ES_config = new StoreConfig();
        ES_config.setAllowCreate(true);
        ES_config.setTransactional(true);

        duraConfig = new Durability(Durability.SyncPolicy.WRITE_NO_SYNC,null,null);

        SeqConfig = new SequenceConfig();
        SeqConfig.setAllowCreate(true);
        SeqConfig.setInitialValue(1);
        //SeqConfig.setCacheSize(100);
        //SeqConfig.setAutoCommitNoSync(true);

        B2timer = new Timer(true);
        B2Task = new RenewB2Auth();
        B2timer.schedule(B2Task, 0, 82800000);
    }

    @Override
    public void destroy() {
        B2Task.cancel();
        B2timer.cancel();
    }

    public static int getDoDbWrite() {
        return doDbWrite;
    }

    public static void setDoDbWrite(int doDbWrite) {
        InitialServlet.doDbWrite = doDbWrite;
    }

    public static String getBackblazeB2Downloadurl() {
        return BACKBLAZE_B2_DOWNLOADURL;
    }

    public static String getBackblazeB2Authcode() {
        return BACKBLAZE_B2_AUTHCODE;
    }

    public static String getBackblazeB2Apiurl() {
        return BACKBLAZE_B2_APIURL;
    }

    public static void setBackblazeB2Authcode(String backblazeB2Authcode) {
        BACKBLAZE_B2_AUTHCODE = backblazeB2Authcode;
    }

    public static void setBackblazeB2Apiurl(String backblazeB2Apiurl) {
        BACKBLAZE_B2_APIURL = backblazeB2Apiurl;
    }

    public static void setBackblazeB2Downloadurl(String backblazeB2Downloadurl) {
        BACKBLAZE_B2_DOWNLOADURL = backblazeB2Downloadurl;
    }

    public static void setMaxUploadGuest(int maxUploadGuest) {
        MAX_UPLOAD_GUEST = maxUploadGuest;
    }

    public static void setMaxUploadMember(int maxUploadMember) {
        MAX_UPLOAD_MEMBER = maxUploadMember;
    }

    public static int convStringCatToInteger(String category) {

        switch(category) {
            case "alpha":
                return 1;
            case "bravo":
                return 2;
            case "charlie":
                return 3;
            case "delta":
                return 4;
            default:
                return 0;
        }
    }

    public static String convIntegerCatToString(Integer category) {

        switch(category) {
            case 1:
                return "alpha";
            case 2:
                return "bravo";
            case 3:
                return "charlie";
            case 4:
                return "delta";
            default:
                return "all";
        }
    }


    public static boolean validateNullParameter(String request_something) {

        if (request_something == null)
            return false;
        else if (request_something == "null")
            return false;
        else if (request_something == "")
            return false;
        else
            return true;
    }

}
