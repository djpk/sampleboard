package com.webboard.main.db;

import com.webboard.main.db.dao.CommentDAO;
import com.webboard.main.db.entity.Comment;
import com.sleepycat.je.LockMode;
import com.sleepycat.persist.EntityCursor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static com.sleepycat.je.CursorConfig.READ_UNCOMMITTED;


@WebServlet(name = "CommentLister", urlPatterns = "/board/comment_list")
public class CommentLister extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        int postno = Integer.parseInt(request.getParameter("postno"));
        int commentCount = 0;
        int commentPage = 0;
        int i = 0;

        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();
        DbEnvHandler.makeES(2);
        CommentDAO dao = new CommentDAO(DbEnvHandler.getES_comments());
        EntityCursor<Comment> comments = dao.sidx.entities(null, postno, true, postno, true, READ_UNCOMMITTED);

        if (comments.next() != null)
            commentCount = comments.count();
        else
            commentCount = 0;

        int maxPageNum = (commentCount & 63) > 0 ? (commentCount / 64) + 1 : commentCount / 64;

        commentPage = Integer.parseInt(request.getParameter("commentPage"));

        if (commentPage == 0)
            commentPage = maxPageNum;

        out.println("<h5>댓글 " + commentCount + "개</h5>");

        try {

            if (commentCount == 0) {
                out.println("<p class=\"text-center\">댓글이 없습니다.</p>");
            } else {
                Comment comment = comments.first(LockMode.READ_UNCOMMITTED);

                for (i=0; i<(commentPage-1)*64; i++)
                    comment = comments.next(LockMode.READ_UNCOMMITTED);

                for (i=0; i<64 && comment != null; i++) {
                    out.println("<div class=\"comment-wrapper border border-top-0 border-left-0 border-right-0\">");
                    out.println("<div class=\"comment-content row no-gutters\">" + comment.getComment_content() + "</div>");
                    out.println("<div class=\"comment-info row no-gutters\">");
                    if (comment.getComment_loggined() > 0)
                        out.println("<div class=\"col-6 text-left loggined\">" + comment.getComment_author() + "</div>");
                    else
                        out.println("<div class=\"col-6 text-left\">" + comment.getComment_author() + "&nbsp;(" + comment.getComment_ipFiltered() + ")" + "</div>");
                    out.println("<div class=\"col-6 text-right\">" + comment.getComment_date() + "&nbsp;<img src=\"/resources/open-iconic/svg/x.svg\"></div>");
                    out.println("</div>");
                    out.println("</div>");
                    comment = comments.next(LockMode.READ_UNCOMMITTED);
                }

                out.println("<nav id=\"commentPagination\"><ul class=\"pagination justify-content-center\">");

                i = commentPage - ((commentPage & 7) == 0 ? 8 : commentPage & 7) + 1;
                if (i != 1) //Prev 버튼 구현부
                    out.println("<li class=\"page-item\"><a href=\"#\" class=\"page-link\" onclick=\"commentList(" + postno + "," + (i - 1) + ")\">" + "Prev" + "</a></li>");

                for (; ((i & 7) != 0) && i <= maxPageNum; i++) {
                    if (i == commentPage)
                        out.println("<li class=\"page-item active\"><a href=\"#\" class=\"page-link\" onclick=\"commentList(" + postno + "," + i + ")\">" + i + "</a></li>");
                    else
                        out.println("<li class=\"page-item\"><a href=\"#\" class=\"page-link\" onclick=\"commentList(" + postno + "," + i + ")\">" + i + "</a></li>");
                }
                if (i < maxPageNum) {
                    if (i == commentPage)
                        out.println("<li class=\"page-item active\"><a href=\"#\" class=\"page-link\" onclick=\"commentList(" + postno + "," + i + ")\">" + i + "</a></li>");
                    else
                        out.println("<li class=\"page-item\"><a href=\"#\" class=\"page-link\" onclick=\"commentList(" + postno + "," + i + ")\">" + i + "</a></li>");
                    //Next 버튼 구현부
                    out.println("<li class=\"page-item\"><a href=\"#\" class=\"page-link\" onclick=\"commentList(" + postno + "," + (i + 1) + ")\">" + "Next" + "</a></li>");
                }
                out.println("</ul></nav>");
            }
        } catch (Exception e) {
            out.println("<tr>");
            out.println("<td colspan=\"4\" style=\"text-align: center;\">댓글을 불러오는 도중 오류가 발생했습니다.</th>");
            out.println("</tr>");
            out.println("</tbody></table>");
        } finally {
            comments.close();
            DbEnvHandler.dbclose();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}