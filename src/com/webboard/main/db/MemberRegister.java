package com.webboard.main.db;

import com.nhncorp.lucy.security.xss.XssPreventer;
import com.webboard.main.db.dao.MemberDAO;
import com.webboard.main.db.entity.Member;
import com.webboard.main.exceptions.MsgCall;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockConflictException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static com.webboard.main.web.CheckInputValidate.checkRegisterFormCheck;
import static com.sleepycat.je.LockMode.READ_UNCOMMITTED;


@WebServlet(name = "MemberRegister", urlPatterns = "/member/register")
public class MemberRegister extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();
        int check = 0;
        String username = "", mailaddress="", nickname = "", password = "", gRecaptchaResponse = "", auth = "";
        try {

            username = request.getParameter("memberUsername");
            nickname = XssPreventer.escape(request.getParameter("memberNickname"));
            mailaddress = request.getParameter("memberMailAddress");
            password = request.getParameter("memberPassword");

            nickname = nickname.replaceAll("\\p{Z}","");

            gRecaptchaResponse = request.getParameter("g-recaptcha-response");

            if (!checkRegisterFormCheck(username, nickname, password, gRecaptchaResponse))
                throw new MsgCall("잘못된 요청이 포함되어, 회원 가입을 진행할 수 없습니다. 사이트 관리자에게 문의하십시오.");

            DbEnvHandler.makeES(0);
            MemberDAO dao = new MemberDAO(DbEnvHandler.getES_members());

            if (dao.sidx_username.get(null, username, READ_UNCOMMITTED) != null) // 1
                check = check + 1;
            else if (dao.sidx_nickname.get(null, nickname, READ_UNCOMMITTED) != null) // 3
                check = check + 3;
            else { /*DO NOTHING */ }

            if (check == 0) {
                //검증 완료
            } else if (check == 1) {
                // 아이디 중복
                throw new MsgCall("입력하신 로그인 ID가 존재합니다.", check);
            } else if (check == 3) {
                // 필명만 중복
                throw new MsgCall("입력하신 필명이 존재합니다.", check);
            } else if (check == 4) {
                // 아이디, 필명 중복
                throw new MsgCall("입력하신 로그인 ID, 필명이 존재합니다.", check);
            } else if (check == 6) {
                // 아이디, 이메일 중복
                throw new MsgCall("입력하신 로그인 ID, 이메일이 존재합니다.", check);
            } else if (check == 8) {
                // 필명, 이메일 중복
                throw new MsgCall("입력하신 필명, 이메일이 존재합니다.", check);
            } else {
                //전부 중복
                throw new MsgCall("입력하신 로그인 ID, 필명, 이메일이 모두 존재합니다. 비밀번호를 잊으셨습니까? 사이트 관리자에게 문의하십시오.", check);
            }

            Member theMember = new Member();
            LocalDateTime theTime = LocalDateTime.now();
            auth = UUID.randomUUID().toString().replace("-","") + sha1Hex(username);

            theMember.setMember_nickname(nickname); //XSS Filtered
            theMember.setMember_username(username);
            theMember.setMember_mailaddress(mailaddress);
            theMember.setMember_password(password);
            theMember.setMember_authtoken(auth);

            theMember.setMember_mailaddress(auth);
            theMember.setMember_status(2); // 2 == NOT TRUSTED

            theMember.setMember_joindate(theTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

            theMember.setMember_permission(1); // 1 == NORMAL

            int tx_retry = 0;

            while (tx_retry < 100) {
                try {
                    DbEnvHandler.dbTxStart();
                    dao.pidx.put(theMember);
                    DbEnvHandler.dbTxCommit();
                    break;
                } catch (LockConflictException lce) {
                    try {
                        DbEnvHandler.dbTxAbort();
                        tx_retry++;
                    } catch (DatabaseException dbe) {
                        response.sendRedirect("/errors?msg=tx_abort_failed");
                        break;
                    }
                } catch (DatabaseException dbe) {
                    response.sendRedirect("/errors?msg=tx_failed");
                    break;
                }
            }
            response.sendRedirect("/");

        } catch (MsgCall dpe) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("register.jsp");
            if (dpe.param == 1) {
                // 아이디만 중복
                request.setAttribute("mailaddress", mailaddress);
                request.setAttribute("nickname", nickname);
            } else if (dpe.param == 3) {
                // 필명만 중복
                request.setAttribute("mailaddress", mailaddress);
                request.setAttribute("username", username);
            } else if (dpe.param == 5) {
                // 이메일만 중복
                request.setAttribute("username", username);
                request.setAttribute("nickname", nickname);
            } else if (dpe.param == 4) {
                // 아이디, 필명 중복
                request.setAttribute("mailaddress", mailaddress);
            } else if (dpe.param == 6) {
                // 아이디, 이메일 중복
                request.setAttribute("nickname", nickname);
            } else if (dpe.param == 8) {
                // 필명, 이메일 중복
                request.setAttribute("username", username);
            } else {
                //전부 중복
            }
            request.setAttribute("msg", dpe.msg);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            response.sendRedirect("/errors?msg=critical_error");
        } finally {
            DbEnvHandler.dbclose();
        }
    } // End of DoPost

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.sendRedirect("/member/register.jsp");
        } catch (Exception e) {
            response.sendRedirect("/errors?msg=critical_error");
        }
    }
}