package com.webboard.main.db;

import com.nhncorp.lucy.security.xss.XssPreventer;
import com.nhncorp.lucy.security.xss.XssSaxFilter;
import com.webboard.main.db.dao.AttachmentDAO;
import com.webboard.main.db.dao.PostDAO;
import com.webboard.main.db.entity.Attachment;
import com.webboard.main.db.entity.Post;
import com.webboard.main.exceptions.MsgCall;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockConflictException;
import com.sleepycat.je.LockMode;
import com.webboard.main.web.CheckInputValidate;
import com.webboard.main.web.InitialServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;



@WebServlet(name = "PostWriter", urlPatterns = "/write")
public class PostWriter extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();
        int loggined = -1;

        String author = request.getParameter("postAuthor");
        String password = request.getParameter("postPassword");
        String subject = request.getParameter("postSubject");
        String contents = request.getParameter("editor");

        String[] attachment_saved;

        if (request.getParameter("saved") != null && !request.getParameter("saved").equals("") && !request.getParameter("saved").equals("null"))
            attachment_saved = request.getParameterValues("saved");
        else
            attachment_saved = null;

        try {
            author = XssPreventer.escape(author);
            subject = XssPreventer.escape(subject);

            subject = subject.trim();
            subject = subject.replaceAll("\u3000", "");

            XssSaxFilter filter = XssSaxFilter.getInstance("lucy-xss-superset-sax.xml", true);
            contents = filter.doFilter(contents);

            String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

            if (!CheckInputValidate.checkWriteFormCheck(author, password, subject, contents, gRecaptchaResponse))
                throw new MsgCall("잘못된 요청이 포함되어, 글쓰기를 진행할 수 없습니다. 양식에 맞게 글을 다시 작성해주세요.");


            HttpSession UserSession = request.getSession();

            if (UserSession.getAttribute("userid") != null) {
                author = UserSession.getAttribute("usernickname").toString();
                password = UserSession.getAttribute("userauth").toString();
                loggined = (Integer) UserSession.getAttribute("userid");

            }

            if (attachment_saved != null && UserSession.getAttribute("userid") != null) {
                if (attachment_saved.length > InitialServlet.MAX_UPLOAD_MEMBER)
                    throw new MsgCall("첨부파일 최대 허용 개수 : "+ InitialServlet.MAX_UPLOAD_MEMBER + "개를 초과했습니다.");
            } else if (attachment_saved != null) {
                if (attachment_saved.length > InitialServlet.MAX_UPLOAD_GUEST)
                    throw new MsgCall("첨부파일 최대 허용 개수 : "+ InitialServlet.MAX_UPLOAD_GUEST + "개를 초과했습니다.");
            }

            DbEnvHandler.makeES(1);
            DbEnvHandler.makeES(3);

            PostDAO dao = new PostDAO(DbEnvHandler.getES_posts());
            Post thePost = new Post();

            AttachmentDAO adao = new AttachmentDAO(DbEnvHandler.getES_attachments());
            Attachment theAttach;

            if (attachment_saved != null) {
                thePost.setPost_attachments(attachment_saved);
                for(int i=0; i<attachment_saved.length; i++) {
                    System.out.println(attachment_saved[i].substring(0, attachment_saved[i].indexOf('#')));
                    theAttach = adao.pidx.get(null, attachment_saved[i].substring(0, attachment_saved[i].indexOf('#')), LockMode.READ_UNCOMMITTED);
                    theAttach.setAttach_status(1);
                    adao.pidx.put(theAttach);
                }
            } else {
                thePost.setPost_attachments(null);
            }

            LocalDateTime theTime = LocalDateTime.now();

            thePost.setPost_loginned(loggined);
            thePost.setPost_author(author);
            thePost.setPost_password(password);
            thePost.setPost_subject(subject);
            thePost.setPost_content(contents);
            thePost.setPost_date(theTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            thePost.setPost_category(InitialServlet.convStringCatToInteger(request.getParameter("postCategory")));
            System.out.println(InitialServlet.convStringCatToInteger(request.getParameter("postCategory")));
            thePost.setPost_status(1);
            thePost.setPost_viewcounts(0);
            thePost.setPost_thumbsup(0);

            String ip = request.getHeader("X-Forwarded-For");

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
            thePost.setPost_ipaddress(ip);


            int tx_retry = 0;

            while (tx_retry < 100) {
                try {
                    DbEnvHandler.dbTxStart();
                    dao.pidx.put(thePost);
                    DbEnvHandler.dbTxCommit();
                    break;
                } catch (LockConflictException lce) {
                    try {
                        DbEnvHandler.dbTxAbort();
                        tx_retry++;
                    } catch (DatabaseException dbe) {
                        response.sendRedirect("/errors?msg=tx_abort_failed");
                        break;
                    }
                } catch (DatabaseException dbe) {
                    dbe.printStackTrace();
                    response.sendRedirect("/errors?msg=tx_failed");
                    break;
                }
            }

            response.sendRedirect("/?cat=" + request.getParameter("postCategory") + "&page=1");
        } catch (MsgCall dge) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/board/write.jsp?cat=" + request.getParameter("postCategory") + "&page=1");
            if (loggined > 0) {
                request.setAttribute("subject", subject);
                request.setAttribute("content", contents);
                request.setAttribute("postAttachments", attachment_saved);
            } else {
                request.setAttribute("author", author);
                request.setAttribute("subject", subject);
                request.setAttribute("content", contents);
                request.setAttribute("postAttachments", attachment_saved);
            }
            request.setAttribute("msg", dge.msg);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            RequestDispatcher dispatcher = request.getRequestDispatcher("/board/write.jsp?cat=" + request.getParameter("postCategory") + "&page=1");
            if (loggined > 0) {
                request.setAttribute("content", contents);
                request.setAttribute("subject", subject);
                request.setAttribute("postAttachments", attachment_saved);
            } else {
                request.setAttribute("author", author);
                request.setAttribute("subject", subject);
                request.setAttribute("content", contents);
                request.setAttribute("postAttachments", attachment_saved);
            }
            request.setAttribute("msg", "글쓰기 도중 알 수 없는 오류가 발생했습니다. 본문을 백업한 후 나중에 다시 시도해주십시오.");
            dispatcher.forward(request, response);
        } finally {
            DbEnvHandler.dbclose();
        }
    } // End of DoPost
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.sendRedirect("/board/write.jsp");
        } catch (Exception e) {
            response.sendRedirect("/errors?msg=critical_error");
        }
    }

}
