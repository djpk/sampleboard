package com.webboard.main.db;

import com.nhncorp.lucy.security.xss.XssPreventer;
import com.webboard.main.db.dao.CommentDAO;
import com.webboard.main.db.dao.PostDAO;
import com.webboard.main.db.entity.Comment;
import com.webboard.main.db.entity.Post;
import com.webboard.main.exceptions.MsgCall;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockConflictException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.webboard.main.web.CheckInputValidate.checkCommentFormCheck;


@WebServlet(name = "CommentWriter", urlPatterns = "/board/comment_write")
public class CommentWriter extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();
        HttpSession UserSession = request.getSession();
        String author = "", password = "", content = "", gRecaptchaResponse = "";
        int loggined = -1;
        int postNode = Integer.parseInt(request.getParameter("postno"));

        try {

            author = XssPreventer.escape(request.getParameter("commentAuthor"));
            password = request.getParameter("commentPassword");

            DbEnvHandler.makeES(1); //MAKE POST ENTITY
            PostDAO pdao = new PostDAO(DbEnvHandler.getES_posts());
            Post thePost = pdao.pidx.get(postNode);

            content = XssPreventer.escape(request.getParameter("commentContent"));

            gRecaptchaResponse = request.getParameter("g-recaptcha-response");

            if (!checkCommentFormCheck(author, password, content, gRecaptchaResponse, thePost.getPost_status()))
                throw new MsgCall("잘못된 요청이 포함되어, 글쓰기를 진행할 수 없습니다. 양식에 맞게 글을 다시 작성해주세요.");

            if (UserSession.getAttribute("userid") != null) {
                author = UserSession.getAttribute("usernickname").toString();
                password = UserSession.getAttribute("userauth").toString();
                loggined = (Integer) UserSession.getAttribute("userid");
            }

            LocalDateTime theTime = LocalDateTime.now();

            Comment theComment = new Comment();
            DbEnvHandler.makeES(2); //COMMENT ENTITY
            CommentDAO dao = new CommentDAO(DbEnvHandler.getES_comments());

            theComment.setComment_author(author);
            theComment.setComment_password(password);
            theComment.setComment_content(content);
            theComment.setComment_postnode(postNode); //VERY MUCH IMPORTANT.
            theComment.setComment_date(theTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

            theComment.setComment_loggined(loggined);
            theComment.setComment_status(1);

            String ip = request.getHeader("X-Forwarded-For");

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
            theComment.setComment_ipaddress(ip);

            int tx_retry = 0;

            while ( tx_retry < 100 ) {
                try {
                    DbEnvHandler.dbTxStart();
                    dao.pidx.put(theComment);
                    pdao.pidx.put(thePost);
                    DbEnvHandler.dbTxCommit();
                    break;
                } catch (LockConflictException lce) {
                    try {
                        DbEnvHandler.dbTxAbort();
                        tx_retry++;
                    } catch (DatabaseException dbe) {
                        dbe.printStackTrace();
                        break;
                    }
                } catch (DatabaseException dbe) {
                    dbe.printStackTrace();
                    break;
                }
            }
            response.sendRedirect("/view?cat=" + request.getParameter("category") + "&no=" + Integer.toString(postNode) + "&page=" + request.getParameter("page"));

        } catch (MsgCall dge) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view?no=" + Integer.toString(postNode) + "&page=" + request.getParameter("page"));
            request.setAttribute("msg", dge.msg);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view?no=" + Integer.toString(postNode) + "&page=" + request.getParameter("page"));
            request.setAttribute("msg", "글쓰기 도중 알 수 없는 오류가 발생했습니다. 본문을 백업한 후 나중에 다시 시도해주십시오.");
            dispatcher.forward(request, response);
        } finally {
            DbEnvHandler.dbclose();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.sendRedirect("/list?cat=all");
        } catch (Exception e) {
            response.sendRedirect("/errors/list_error.html");
        }
    }
}
