package com.webboard.main.db;

import com.sleepycat.je.*;
import com.sleepycat.persist.EntityStore;
import com.webboard.main.web.InitialServlet;

import java.io.File;


public class EnvHandler {

    private Environment DbEnv = null;
    private EntityStore ES_members = null;
    private EntityStore ES_posts = null;
    private EntityStore ES_comments = null;
    private EntityStore ES_contents = null;
    private EntityStore ES_attachments = null;
    private Transaction txn = null;

    public void dbinit() {
        try {
            DbEnv = new Environment(new File(InitialServlet.PATH_TO_DB), InitialServlet.envConfig);
        } catch (DatabaseException dbe) {
            dbe.printStackTrace();
            DbEnv.close();
        }
    }

    public void makeES(int entitychoose) {
        switch (entitychoose)
        {
            case 0: //Using for members.
                ES_members = new EntityStore(DbEnv, "members", InitialServlet.ES_config);
                ES_members.setSequenceConfig("member_id", InitialServlet.SeqConfig);
                break;
            case 1: //Using for Post
                ES_posts = new EntityStore(DbEnv, "posts", InitialServlet.ES_config);
                ES_posts.setSequenceConfig("post_id", InitialServlet.SeqConfig);
                break;
            case 2: //Using for Comment Writing & Listing & Viewing
                ES_comments = new EntityStore(DbEnv, "comments", InitialServlet.ES_config);
                ES_comments.setSequenceConfig("comment_id", InitialServlet.SeqConfig);
                break;
            case 3:
                ES_attachments = new EntityStore(DbEnv, "attachments", InitialServlet.ES_config);
            default:
                break;
        }
    }

    public void dbclose() {
        try {
            if (DbEnv != null) {
                if (ES_members != null)
                    ES_members.close();
                if (ES_posts != null)
                    ES_posts.close();
                if (ES_comments != null)
                    ES_comments.close();
                if (ES_contents != null)
                    ES_contents.close();
                if (ES_attachments != null)
                    ES_attachments.close();
            }
        } catch (DatabaseException dbe) {
            dbe.printStackTrace();
        } finally {
            DbEnv.close();
        }
    }

    public void dbTxStart() {
        TransactionConfig txconfig = new TransactionConfig();
        txconfig.setDurability(InitialServlet.duraConfig);
        txn = DbEnv.beginTransaction(null, txconfig);
    }

    public Transaction getTxn() {
        return txn;
    }

    public void dbTxCommit() {
        txn.commit();
    }

    public void dbTxAbort() {
        if (txn != null)
            txn.abort();
    }

    public EntityStore getES_members() {
        return ES_members;
    }

    public EntityStore getES_posts() {
        return ES_posts;
    }

    public EntityStore getES_comments() {
        return ES_comments;
    }

    public EntityStore getES_contents() {
        return ES_contents;
    }

    public EntityStore getES_attachments() {
        return ES_attachments;
    }
}
