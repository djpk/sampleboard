package com.webboard.main.db;

import com.webboard.main.db.dao.CommentDAO;
import com.webboard.main.db.dao.PostDAO;
import com.webboard.main.db.entity.Comment;
import com.webboard.main.db.entity.Post;
import com.sleepycat.je.LockMode;
import com.sleepycat.persist.EntityCursor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static com.sleepycat.je.CursorConfig.READ_UNCOMMITTED;
import static java.lang.Math.toIntExact;


@WebServlet(name = "PostLister", urlPatterns = "/list")
public class PostLister extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int post_count = 0;
        String category = "all";
        int pageNum = 1;
        int maxPageNum = 0;
        int i = 0;
        int postno = 0;

        if (request.getParameter("cat") != null && request.getParameter("cat") != "null")
            category = request.getParameter("cat");
        if (request.getParameter("page") != null && request.getParameter("page") != "null")
            pageNum = Integer.parseInt(request.getParameter("page"));
        if (request.getParameter("postno") != null && request.getParameter("postno") != "null")
            postno = Integer.parseInt(request.getParameter("postno"));


        PrintWriter out = response.getWriter();
        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();

        DbEnvHandler.makeES(1);
        DbEnvHandler.makeES(2);

        PostDAO dao = new PostDAO(DbEnvHandler.getES_posts());
        CommentDAO cdao = new CommentDAO(DbEnvHandler.getES_comments());
        Comment comment;
        Post post;

        EntityCursor<Post> posts; // 커서 생성 *NPE 주의*
        EntityCursor<Comment> comments;

        switch (category) {
            case "apple":
                posts = dao.sidx_category.entities(null, 1, true, 1, true, READ_UNCOMMITTED);
                if (posts.next() != null)
                    post_count = posts.count();
                else
                    post_count = 0;
                break;
            case "banana":
                posts = dao.sidx_category.entities(null, 2, true, 2, true, READ_UNCOMMITTED);
                if (posts.next() != null)
                    post_count = posts.count();
                else
                    post_count = 0;
                break;
            case "cherry":
                posts = dao.sidx_category.entities(null, 3, true, 3, true, READ_UNCOMMITTED);
                if (posts.next() != null)
                    post_count = posts.count();
                else
                    post_count = 0;
                break;
            default:
                posts = dao.pidx.entities(null, READ_UNCOMMITTED);
                post_count = toIntExact(dao.pidx.count()); // 전체 개수 세어주기
                break;
        }

        try {
            out.println("<div id=\"board_list_container\">");
            out.println("<table id=\"board_list\" class=\"table table-sm table-hover\"><thead><tr>");
            out.println("<th style=\"width: 8%; text-align: center;\">번호</th>");
            out.println("<th style=\"width: 52%; text-align: center;\">제목</th>");
            out.println("<th style=\"width: 12%; text-align: center;\">작성자</th>");
            out.println("<th style=\"width: 12%; text-align: center;\">날짜</th>");
            out.println("<th style=\"width: 8%; text-align: center;\">조회수</th>");
            out.println("<th style=\"width: 8%; text-align: center;\">추천</th>");
            out.println("</tr></thead><tbody>");

            if (post_count == 0)
                out.println("<tr style=\"text-align: center;\"><td colspan=\"6\">글이 없습니다.</td></tr>");
            else {
                // (현재 페이지 - 1 ) * 32 NEXT할 횟수
                post = posts.last(LockMode.READ_UNCOMMITTED);

                for (i=0; i<(pageNum-1)*32; i++)
                    post = posts.prev(LockMode.READ_UNCOMMITTED);

                for (i=0; i<32 && post != null; i++) {
                    out.println("<tr>");
                    if (postno == post.getPost_id())
                        out.println("<td class=\"list_id text-center\">" + "&raquo;" + "</th>"); //글번호
                    else
                        out.println("<td class=\"list_id text-center\">" + post.getPost_id() + "</th>"); //글번호
                    comments = cdao.sidx.entities(null, post.getPost_id(), true, post.getPost_id(), true, READ_UNCOMMITTED);
                    try {
                        if (comments.next() != null) {
                            out.println("<td class=\"list_subject\"><a href=\"view?cat=" + category + "&no=" + post.getPost_id() + "&page=" + pageNum + "\">" + post.getPost_subject() + "</a>&nbsp;<sub>[" + comments.count() + "]</sub></th>");
                        } else {
                            out.println("<td class=\"list_subject\"><a href=\"view?cat=" + category + "&no=" + post.getPost_id() + "&page=" + pageNum + "\">" + post.getPost_subject() + "</a></th>");
                        }
                    } finally {
                        comments.close();
                    }
                    if (post.getPost_loginned() > 0)
                        out.println("<td class=\"list_author loggined text-center\">" + post.getPost_author() + "</th>");
                    else
                        out.println("<td class=\"list_author text-center\">" + post.getPost_author() + "</th>");
                    out.println("<td class=\"list_date text-center\">" + post.getPost_date().substring(0, 10) + "</th>");
                    out.println("<td class=\"list_viewcounts text-center\">" + post.getPost_viewcounts() + "</th>");
                    out.println("<td class=\"list_thumbsup text-center\">" + post.getPost_thumbsup() + "</th>");
                    out.println("</tr>");
                    post = posts.prev(LockMode.READ_UNCOMMITTED);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            out.println("<tr><td colspan=\"6\">글 목록을 불러오던 중 오류가 발생했습니다.</td></tr>");
        } finally {
            posts.close();
            DbEnvHandler.dbclose();
            out.println("</tbody></table></div>");
        }

        /** 리스트 구현부 끝남.
         *  아래에서 부터 페이징 구현부
         *  */

        maxPageNum = (post_count & 31) > 0 ? (post_count / 32) + 1 : post_count / 32;

        out.println("<nav><ul class=\"pagination justify-content-center\">");

        i = pageNum - ((pageNum & 7) == 0 ? 8 : pageNum & 7) + 1;

        if (i != 1) //Prev 버튼 구현부
            out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"/?cat=" + category + "&page=" + (i - 1) + "\">" + "Prev" + "</a></li>");

        for (; ((i & 7) != 0) && i <= maxPageNum; i++) {
            if (i == pageNum)
                out.println("<li class=\"page-item active\"><a class=\"page-link\" href=\"/?cat=" + category + "&page=" + i + "\">" + i + "</a></li>");
            else
                out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"/?cat=" + category + "&page=" + i + "\">" + i + "</a></li>");
        }
        if (i < maxPageNum) {
            if (i == pageNum)
                out.println("<li class=\"page-item active\"><a class=\"page-link\" href=\"/?cat=" + category + "&page=" + i + "\">" + i + "</a></li>");
            else
                out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"/?cat=" + category + "&page=" + i + "\">" + i + "</a></li>");
            //Next 버튼 구현부
            out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"/?cat=" + category + "&page=" + (i + 1) + "\">" + "Next" + "</a></li>");
        }

        out.println("</ul></nav>");
    }
}
