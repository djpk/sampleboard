package com.webboard.main.db;

import com.nhncorp.lucy.security.xss.XssPreventer;
import com.nhncorp.lucy.security.xss.XssSaxFilter;
import com.webboard.main.db.dao.AttachmentDAO;
import com.webboard.main.db.dao.PostDAO;
import com.webboard.main.db.entity.Attachment;
import com.webboard.main.db.entity.Post;
import com.webboard.main.exceptions.MsgCall;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockConflictException;
import com.sleepycat.je.LockMode;
import com.webboard.main.web.InitialServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.webboard.main.web.CheckInputValidate.checkWriteFormCheck;


@WebServlet(name = "PostModifier", urlPatterns = "/modify")
public class PostModifier extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();
        HttpSession UserSession = request.getSession();

        String author = request.getParameter("postAuthor");
        String oldpassword = request.getParameter("postOldPassword");
        String password;
        String subject = request.getParameter("postSubject");
        String contents = request.getParameter("editor");
        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

        String[] attachment_saved;
        String[] attachment_fucked;

        if (request.getParameter("saved") != null && !request.getParameter("saved").equals("") && !request.getParameter("saved").equals("null"))
            attachment_saved = request.getParameterValues("saved");
        else
            attachment_saved = null;

        if (request.getParameter("fucked") != null && !request.getParameter("fucked").equals("") && !request.getParameter("fucked").equals("null"))
            attachment_fucked = request.getParameterValues("fucked");
        else
            attachment_fucked = null;

        int loggined = -1;
        String postNo = request.getParameter("postNo");
        String postPage = request.getParameter("postPage");
        String postCategory = request.getParameter("postCategory");

        try {


            //비회원일 경우
            author = XssPreventer.escape(author);

            System.out.println("postNewPassword : " + request.getParameter("postNewPassword"));

            if (request.getParameter("postNewPassword") == null || request.getParameter("postNewPassword").equals("null") || request.getParameter("postNewPassword").equals(""))
                password = oldpassword;
            else
                password = request.getParameter("postNewPassword");

            if (request.getParameter("postPage").equals("") && request.getParameter("postPage").equals("null") && request.getParameter("postPage") == null)
                throw new MsgCall("잘못된 요청 입니다.");

            subject = XssPreventer.escape(subject);

            subject = subject.trim();
            subject = subject.replaceAll("\u3000", "");

            XssSaxFilter filter = XssSaxFilter.getInstance("lucy-xss-superset-sax.xml", true);
            contents = filter.doFilter(contents);

            if (!checkWriteFormCheck(author, password, subject, contents, gRecaptchaResponse))
                throw new MsgCall("잘못된 요청이 포함되어, 글쓰기를 진행할 수 없습니다. 양식에 맞게 글을 다시 작성해주세요.");



            if (UserSession.getAttribute("userid") != null) {
                author = UserSession.getAttribute("usernickname").toString();
                oldpassword = UserSession.getAttribute("userauth").toString();
                password = oldpassword;
                loggined = (Integer) UserSession.getAttribute("userid");
            }

            if (attachment_saved != null && UserSession.getAttribute("userid") != null) {
                if (attachment_saved.length > InitialServlet.MAX_UPLOAD_MEMBER)
                    throw new MsgCall("첨부파일 최대 허용 개수 : "+ InitialServlet.MAX_UPLOAD_MEMBER + "개를 초과했습니다.");
            } else if (attachment_saved != null) {
                if (attachment_saved.length > InitialServlet.MAX_UPLOAD_GUEST)
                    throw new MsgCall("첨부파일 최대 허용 개수 : "+ InitialServlet.MAX_UPLOAD_GUEST + "개를 초과했습니다.");
            }

            DbEnvHandler.makeES(1);
            DbEnvHandler.makeES(3);

            PostDAO dao = new PostDAO(DbEnvHandler.getES_posts());
            Post thePost = dao.pidx.get(null, Integer.parseInt(postNo), LockMode.READ_UNCOMMITTED);

            if (!thePost.checkPost_password(oldpassword))
                throw new MsgCall("입력한 비밀번호가 일치하지 않습니다. 비밀번호를 확인 후 다시 진행해주십시오.");

            AttachmentDAO adao = new AttachmentDAO(DbEnvHandler.getES_attachments());
            Attachment theAttach;

            if (attachment_saved != null) {
                thePost.setPost_attachments(attachment_saved);
                for(int i=0; i<attachment_saved.length; i++) {
                    theAttach = adao.pidx.get(null, attachment_saved[i].substring(0, attachment_saved[i].indexOf('#')), LockMode.READ_UNCOMMITTED);
                    theAttach.setAttach_status(1);
                    adao.pidx.put(theAttach);
                }
            } else {
                thePost.setPost_attachments(null);
            }

            if (attachment_fucked != null) {
                for(int i=0; i<attachment_fucked.length; i++) {
                    theAttach = adao.pidx.get(null, attachment_fucked[i].substring(0, attachment_fucked[i].indexOf('#')), LockMode.READ_UNCOMMITTED);
                    theAttach.setAttach_status(0);
                    adao.pidx.put(theAttach);
                }
            }

            thePost.setPost_password(password);

            LocalDateTime theTime = LocalDateTime.now();

            thePost.setPost_loginned(loggined);

            thePost.setPost_author(author);
            thePost.setPost_subject(subject);
            thePost.setPost_content(contents);

            thePost.setPost_date(theTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));


            int tx_retry = 0;

            while (tx_retry < 100) {
                try {
                    DbEnvHandler.dbTxStart(); //Transaction Started!
                    dao.pidx.put(thePost);
                    DbEnvHandler.dbTxCommit();
                    break;
                } catch (LockConflictException lce) {
                    try {
                        DbEnvHandler.dbTxAbort();
                        tx_retry++;
                    } catch (DatabaseException dbe) {
                        response.sendRedirect("/errors?msg=tx_abort_failed");
                        break;
                    }
                } catch (DatabaseException dbe) {
                    dbe.printStackTrace();
                    response.sendRedirect("/errors?msg=tx_failed");
                    break;
                }
            }

            response.sendRedirect("/view?cat=" + postCategory + "&no=" + postNo + "&page=" + postPage);
        } catch (MsgCall dge) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/board/modify.jsp?cat=" + postCategory + "&no=" + postNo + "&page=" + postPage);
            if (loggined > 0) {
                request.setAttribute("subject", subject);
                request.setAttribute("content", contents);
                request.setAttribute("postNo", postNo);
                request.setAttribute("postAttachments", attachment_saved);
                request.setAttribute("postAttachments_fucked", attachment_fucked);
            } else {
                request.setAttribute("author", author);
                request.setAttribute("subject", subject);
                request.setAttribute("content", contents);
                request.setAttribute("postNo", postNo);
                request.setAttribute("postAttachments", attachment_saved);
                request.setAttribute("postAttachments_fucked", attachment_fucked);
            }
            request.setAttribute("msg", dge.msg);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/board/modify.jsp?cat=" + postCategory + "&no=" + postNo + "&page=" + postPage);
            if (loggined > 0) {
                request.setAttribute("content", contents);
                request.setAttribute("subject", subject);
                request.setAttribute("postNo", postNo);
                request.setAttribute("postAttachments", attachment_saved);
                request.setAttribute("postAttachments_fucked", attachment_fucked);
            } else {
                request.setAttribute("author", author);
                request.setAttribute("subject", subject);
                request.setAttribute("content", contents);
                request.setAttribute("postNo", postNo);
                request.setAttribute("postAttachments", attachment_saved);
                request.setAttribute("postAttachments_fucked", attachment_fucked);
            }
            request.setAttribute("msg", "글쓰기 도중 알 수 없는 오류가 발생했습니다. 본문을 백업한 후 나중에 다시 시도해주십시오.");
            dispatcher.forward(request, response);
        } finally {
            DbEnvHandler.dbclose();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/");
    }
}
