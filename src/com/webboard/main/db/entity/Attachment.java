package com.webboard.main.db.entity;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.SecondaryKey;

import static com.sleepycat.persist.model.Relationship.MANY_TO_ONE;


@Entity
public class Attachment {

    @PrimaryKey
    String attach_id;

    String attach_fname;

    @SecondaryKey(relate = MANY_TO_ONE)
    int attach_status; // 0 - 대기큐, 1 - 사용중

    public String getAttach_id() {
        return attach_id;
    }

    public void setAttach_id(String attach_id) {
        this.attach_id = attach_id;
    }

    public String getAttach_fname() {
        return attach_fname;
    }

    public void setAttach_fname(String attach_fname) {
        this.attach_fname = attach_fname;
    }

    public int getAttach_status() {
        return attach_status;
    }

    public void setAttach_status(int attach_status) {
        this.attach_status = attach_status;
    }
}
