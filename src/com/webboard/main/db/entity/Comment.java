package com.webboard.main.db.entity;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.SecondaryKey;

import static com.sleepycat.persist.model.Relationship.MANY_TO_ONE;


@Entity
public class Comment {

    @PrimaryKey(sequence = "comment_id")
    private int comment_id;

    @SecondaryKey(relate = MANY_TO_ONE)
    private int comment_postnode;

    @SecondaryKey(relate = MANY_TO_ONE)
    private int comment_loggined;

    private int comment_status;
    private String comment_author;
    private String comment_password;
    private String comment_content;
    private String comment_date;
    private String comment_ipaddress;

    public String getComment_ipaddress() {
        return comment_ipaddress;
    }

    public void setComment_ipaddress(String comment_ipaddress) {
        this.comment_ipaddress = comment_ipaddress;
    }

    public int getComment_id() {
        return comment_id;
    }

    public int getComment_postnode() {
        return comment_postnode;
    }

    public void setComment_postnode(int comment_postnode) {
        this.comment_postnode = comment_postnode;
    }

    public int getComment_status() {
        return comment_status;
    }

    public void setComment_status(int comment_status) {
        this.comment_status = comment_status;
    }

    public int getComment_loggined() {
        return comment_loggined;
    }

    public void setComment_loggined(int comment_loggined) {
        this.comment_loggined = comment_loggined;
    }

    public String getComment_author() {
        return comment_author;
    }

    public void setComment_author(String comment_author) {
        this.comment_author = comment_author;
    }

    public String getComment_password() {
        return comment_password;
    }

    public void setComment_password(String comment_password) {
        this.comment_password = comment_password;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }

    public String getComment_date() {
        return comment_date;
    }

    public void setComment_date(String comment_date) {
        this.comment_date = comment_date;
    }

    public String getComment_ipFiltered() {
        int dot_count = 0;
        int i = 0;
        for (i=0; i<this.comment_ipaddress.length(); i++) {
            if (dot_count == 2)
                break;
            if (this.comment_ipaddress.charAt(i) == '.' || this.comment_ipaddress.charAt(i) == ':')
                dot_count++;
        }
        return this.comment_ipaddress.substring(0, i-1);
    }
}