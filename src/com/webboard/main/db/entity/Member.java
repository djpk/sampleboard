package com.webboard.main.db.entity;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.SecondaryKey;
import org.mindrot.jbcrypt.BCrypt;

import static com.sleepycat.persist.model.Relationship.ONE_TO_ONE;



@Entity
public class Member {
    @PrimaryKey(sequence = "member_id")
    private int member_id;

    @SecondaryKey(relate = ONE_TO_ONE)
    private String member_username;

    @SecondaryKey(relate = ONE_TO_ONE)
    private String member_nickname;

    @SecondaryKey(relate = ONE_TO_ONE)
    private String member_mailaddress;

    private String member_authtoken;
    private int member_status;
    private String member_password;
    private int member_permission;
    private String member_joindate;

    public String getMember_authtoken() {
        return member_authtoken;
    }

    public void setMember_authtoken(String member_authtoken) {
        this.member_authtoken = member_authtoken;
    }

    public String getMember_username() {
        return member_username;
    }

    public void setMember_username(String member_username) {
        this.member_username = member_username;
    }

    public int getMember_permission() {
        return member_permission;
    }

    public void setMember_permission(int member_permission) {
        this.member_permission = member_permission;
    }

    public int getMember_id() {
        return member_id;
    }

    public String getMember_nickname() {
        return member_nickname;
    }

    public void setMember_nickname(String member_nickname) {
        this.member_nickname = member_nickname;
    }

    public boolean checkMember_password(String member_password) {
        return BCrypt.checkpw(member_password, this.member_password);
    }

    public void setMember_password(String member_password) {
        this.member_password = BCrypt.hashpw(member_password, BCrypt.gensalt(10));
    }

    public String getMember_mailaddress() {
        return member_mailaddress;
    }

    public void setMember_mailaddress(String member_mailaddress) {
        this.member_mailaddress = member_mailaddress;
    }

    public String getMember_joindate() {
        return member_joindate;
    }

    public void setMember_joindate(String member_joindate) {
        this.member_joindate = member_joindate;
    }

    public int getMember_status() {
        return member_status;
    }

    public void setMember_status(int member_status) {
        this.member_status = member_status;
    }
}
