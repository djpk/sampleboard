package com.webboard.main.db.entity;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.SecondaryKey;
import org.mindrot.jbcrypt.BCrypt;

import static com.sleepycat.persist.model.Relationship.MANY_TO_ONE;


@Entity(version = 1)
public class Post {

    @PrimaryKey(sequence = "post_id")
    private int post_id;

    @SecondaryKey(relate = MANY_TO_ONE)
    private int post_category;

    @SecondaryKey(relate = MANY_TO_ONE)
    private int post_loginned;

    private String post_author;
    private String post_password;
    private String post_subject;
    private String post_content;
    private String post_date;

    private int post_status;  //POST STATUS 0 = DELETED, 1 = ALL OPEN, 2 = CATEGORY_OPEN_ONLY 3 = CENSORED
    private int post_viewcounts;

    private int post_thumbsup;

    private String post_ipaddress;

    private String[] post_attachments;

    public String[] getPost_attachments() {
        return post_attachments;
    }

    public void setPost_attachments(String[] post_attachments) {
        this.post_attachments = post_attachments;
    }

    public int getPost_id() {
        return post_id;
    }

    public int getPost_category() {
        return post_category;
    }

    public void setPost_category(int post_category) {
        this.post_category = post_category;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public int getPost_status() {
        return post_status;
    }

    public void setPost_status(int post_status) {
        this.post_status = post_status;
    }

    public int getPost_viewcounts() {
        return post_viewcounts;
    }

    public void setPost_viewcounts(int post_viewcounts) {
        this.post_viewcounts = post_viewcounts;
    }

    public int getPost_thumbsup() {
        return post_thumbsup;
    }

    public void setPost_thumbsup(int post_thumbsup) {
        this.post_thumbsup = post_thumbsup;
    }

    public String getPost_author() {
        return post_author;
    }

    public void setPost_author(String post_author) {
        this.post_author = post_author;
    }

    public String getPost_subject() {
        return post_subject;
    }

    public void setPost_subject(String post_subject) {
        this.post_subject = post_subject;
    }

    public int getPost_loginned() {
        return post_loginned;
    }

    public void setPost_loginned(int post_loginned) {
        this.post_loginned = post_loginned;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public boolean checkPost_password(String post_password) {
        return BCrypt.checkpw(post_password, this.post_password);
    }

    public void setPost_password(String post_password) {
        this.post_password = BCrypt.hashpw(post_password, BCrypt.gensalt(10));
    }

    public String getPost_ipaddress() {
        return post_ipaddress;
    }

    public void setPost_ipaddress(String post_ipaddress) {
        this.post_ipaddress = post_ipaddress;
    }

    public String getPost_ipFiltered() {
        int dot_count = 0;
        int i = 0;
        for (i=0; i<this.post_ipaddress.length(); i++) {
            if (dot_count == 2)
                break;
            if (this.post_ipaddress.charAt(i) == '.' || this.post_ipaddress.charAt(i) == ':')
                dot_count++;
        }
        return this.post_ipaddress.substring(0, i-1);
    }

}