package com.webboard.main.db;

import com.webboard.main.db.dao.PostDAO;
import com.webboard.main.db.entity.Post;
import com.webboard.main.web.InitialServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(name = "PostReader", urlPatterns = "/view")
public class PostReader extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int postno = 0; String category = "all"; int pageNum = 1;

        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();
        try {
            if (request.getParameter("no") == null)
                throw new Exception();

            if (request.getParameter("cat") != null)
                category = request.getParameter("cat");

            if (request.getParameter("page") != null)
                category = request.getParameter("page");

            postno = Integer.parseInt(request.getParameter("no"));

            DbEnvHandler.makeES(1);

            PostDAO dao = new PostDAO(DbEnvHandler.getES_posts());
            Post thePost = dao.pidx.get(postno);

            if (thePost.getPost_status() == 1) {

                thePost.setPost_viewcounts(thePost.getPost_viewcounts() + 1);
                dao.pidx.put(thePost);
                /* 제목, 이름, 내용, 글쓴시간, 조회수, 추천수, 코맨트수, */

                RequestDispatcher dispatcher = request.getRequestDispatcher("/board/view.jsp");

                request.setAttribute("trusted", thePost.getPost_loginned());
                request.setAttribute("subject", thePost.getPost_subject());
                request.setAttribute("author", thePost.getPost_author());
                request.setAttribute("content", thePost.getPost_content());
                request.setAttribute("date", thePost.getPost_date());
                request.setAttribute("postno", postno);
                request.setAttribute("category", InitialServlet.convIntegerCatToString(thePost.getPost_category()));
                request.setAttribute("page", request.getParameter("page"));
                request.setAttribute("ip", thePost.getPost_ipFiltered());
                request.setAttribute("attachment", thePost.getPost_attachments());

                HttpSession UserSession = request.getSession();
                UserSession.setAttribute("nowviewing", thePost.getPost_id());

                dispatcher.forward(request, response);
            } else {
                response.sendRedirect("/errors?msg=permission_error");
            }
        } catch (Exception e){
            e.printStackTrace();
            response.sendRedirect("/errors?msg=not_found");
        } finally {
            DbEnvHandler.dbclose();
        }
    }
}