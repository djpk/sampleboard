package com.webboard.main.db;

import com.webboard.main.db.dao.PostDAO;
import com.webboard.main.db.entity.Post;
import com.webboard.main.exceptions.MsgCall;
import com.sleepycat.je.LockMode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "PostAuthorizer", urlPatterns = "/getAuth")
public class PostAuthorizer extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession UserSession = request.getSession();

        String postNo = request.getParameter("no");
        String doPost = request.getParameter("do");

        String postpw;

        if (UserSession.getAttribute("userid") != null)
            postpw = UserSession.getAttribute("userauth").toString();
        else //비회원일 경우
            postpw = request.getParameter("postPassword");

        EnvHandler DbEnvHandler = new EnvHandler();
        DbEnvHandler.dbinit();

        try {
            if (postNo.equals("null") || postNo == null || postpw.equals("null") || postpw == null)
                throw new MsgCall("요청값이 잘못되었습니다.");

            int postno_integer = Integer.parseInt(postNo);

            DbEnvHandler.makeES(1);

            PostDAO dao = new PostDAO(DbEnvHandler.getES_posts());
            Post thePost = dao.pidx.get(null, postno_integer, LockMode.READ_UNCOMMITTED);

            if (!thePost.checkPost_password(postpw))
                throw new MsgCall("게시글 비밀번호가 틀립니다.");

            if (thePost.getPost_status() == 1 && doPost.equals("modify")) {

                RequestDispatcher dispatcher = request.getRequestDispatcher("/board/modify.jsp?cat=" + request.getParameter("cat")+ "&no=" + postNo + "&page=" + request.getParameter("page"));

                request.setAttribute("subject", thePost.getPost_subject());
                request.setAttribute("author", thePost.getPost_author());
                request.setAttribute("content", thePost.getPost_content());
                request.setAttribute("postNo", postNo);
                request.setAttribute("postAttachments", thePost.getPost_attachments());

                dispatcher.forward(request, response);

            } else {
                throw new MsgCall("게시글을 수정할 권한이 없습니다.");
            }
        } catch (MsgCall mcall) {
            PrintWriter out = response.getWriter();
            out.println("<script type=\"text/javascript\">");
            out.println("alert('" + mcall.msg +"');");
            out.println("window.history.back();");
            out.println("</script>");
        } finally {
            DbEnvHandler.dbclose();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
