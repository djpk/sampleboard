package com.webboard.main.db.dao;
import com.webboard.main.db.entity.Post;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;


public class PostDAO {
    public PostDAO(EntityStore Estore) throws DatabaseException {
        pidx = Estore.getPrimaryIndex(Integer.class, Post.class);
        sidx_category = Estore.getSecondaryIndex(pidx,Integer.class,"post_category");
        sidx_loggined = Estore.getSecondaryIndex(pidx,Integer.class,"post_loginned");
    }
    public PrimaryIndex<Integer, Post> pidx;
    public SecondaryIndex<Integer, Integer, Post> sidx_category;
    public SecondaryIndex<Integer, Integer, Post> sidx_loggined;
}