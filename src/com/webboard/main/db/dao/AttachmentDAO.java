package com.webboard.main.db.dao;

import com.webboard.main.db.entity.Attachment;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;


public class AttachmentDAO {
    public AttachmentDAO(EntityStore Estore) throws DatabaseException {
        pidx = Estore.getPrimaryIndex(String.class, Attachment.class);
        sidx = Estore.getSecondaryIndex(pidx, Integer.class, "attach_status");
    }
    public PrimaryIndex<String, Attachment> pidx;
    public SecondaryIndex<Integer, String, Attachment> sidx;
}
