package com.webboard.main.db.dao;

import com.webboard.main.db.entity.Comment;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;


public class CommentDAO {

    public PrimaryIndex<Integer, Comment> pidx;
    public SecondaryIndex<Integer, Integer, Comment> sidx;

    public CommentDAO(EntityStore Estore) throws DatabaseException {
        pidx = Estore.getPrimaryIndex(Integer.class, Comment.class);
        sidx = Estore.getSecondaryIndex(pidx, Integer.class, "comment_postnode");
    }

}
