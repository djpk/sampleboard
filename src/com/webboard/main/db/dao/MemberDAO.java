package com.webboard.main.db.dao;
import com.webboard.main.db.entity.Member;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;


public class MemberDAO {
    public MemberDAO(EntityStore Estore) throws DatabaseException {
        pidx = Estore.getPrimaryIndex(Integer.class, Member.class);
        sidx_username = Estore.getSecondaryIndex(pidx,String.class,"member_username");
        sidx_mailaddress = Estore.getSecondaryIndex(pidx,String.class,"member_mailaddress");
        sidx_nickname = Estore.getSecondaryIndex(pidx,String.class,"member_nickname");
    }
    public PrimaryIndex<Integer, Member> pidx;
    public SecondaryIndex<String, Integer, Member> sidx_username;
    public SecondaryIndex<String, Integer, Member> sidx_mailaddress;
    public SecondaryIndex<String, Integer, Member> sidx_nickname;
}