# webboard

A simple web-board written in Java.

# Usage

1. Just drag **ROOT.war**(or you can rename) file to your tomcat **webapps** folder.
2. Run your tomcat and go to page with **/yourdomain.com:8080/**(or domain.com:8080/your_app_name/).

# Testpage

http://13.209.229.109:8080/
